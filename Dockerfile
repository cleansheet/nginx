FROM debian:stretch-slim

MAINTAINER Andras Foldesi <andras.foldesi@novadin.com>

RUN apt-get update && \
    apt-get install -y nginx && \
    sed -i 's/\/etc\/nginx\/sites-enabled\/\*/\/app\/config\/nginx.conf/' /etc/nginx/nginx.conf && \
    echo "\ndaemon off;" >> /etc/nginx/nginx.conf

COPY wait-for-it.sh /usr/bin/

CMD ["nginx"]

EXPOSE 8080 8443
