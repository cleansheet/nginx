# NGINX Docker Image

This is a base NGINX image. Use it in conjunction with novadin/php-fpm.

## Usage

The application files should be attached as a volume from a separate container. The root directory is configured to `/app/www_root`.

```
docker run -d -p 80:8080 -p 443:8443 --link php-fpm:phpserver --volumes-from <app container> --name nginx novadin/nginx
```